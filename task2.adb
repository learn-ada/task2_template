with Text_IO;
with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Image;
with Interfaces.C;

use Ada.Integer_Text_IO;
use Ada.Text_IO;
use Text_IO;
use Interfaces.C;

procedure Task2 is
  bm : Image.Bitmap := Image.Load_Bitmap("texture1.bmp");
begin

  Put_Line("Start");

  Put_Line(Integer'Image(Image.Width(bm)));  -- sorry for too many of 'Image' words ...
  Put_Line(Integer'Image(Image.Height(bm))); -- please differ package and attribute

  -- do something with our image
  --
  for y in bm'First(2) .. bm'Last(2)/2 loop
    for x in bm'First(1) .. bm'Last(1)/2 loop
      bm(x,y) := (255, 255, 0, 0);
    end loop;
  end loop;

  Image.Save_Bitmap(bm, "texture2.png");

  Put_Line("Exit.");

end Task2;

